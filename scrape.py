import requests
from BeautifulSoup import BeautifulSoup
from Queue import Queue

courseQueue = Queue()
version = '1.0'


'''
Function that gets the set of all courses listed in the departmment course catalog and course
requirements list
Param: div - the division (KSAS or WSE)
Param: dept - the department within the division
Returns the set of all courses listed in the department page
'''
def get_courses(div, dept):
	#getting initial html data from JHU e-catalog
	url = 'http://e-catalog.jhu.edu/departments-program-requirements-and-courses/' + div + '/' + dept + '/'
	print url
	response = requests.get(url)
	html = response.content
	soup = BeautifulSoup(html)
	req_tables = soup.findAll('tbody')
	dept_courses = soup.find(id = 'courseinventorycontainer').findAll('p', 
		{'class': 'courseblocktitle'})
	course_list_temp = set()
	course_list = set()
	#get course numbers from undergrad major requirements
	for i in range(0, len(req_tables)):
		req_table = req_tables[i]
		for row in req_table.findAll('tr'):
		    for cell in row.findAll('td'):
		        txt = cell.text.replace('&#160;', '\n').replace('&amp;', '').replace('\n', '')
		        if len(txt) > 3 and (txt[0:3] == 'AS.' or txt[0:3] == 'EN.'):
		        	course_list_temp.add(str(txt).strip())
	#get course numbers from list of department courses
	for course in dept_courses:
		course = str(course).replace('<p class="courseblocktitle"><strong>', '').replace('</strong></p>', ''). replace('.  ', '')
		course = course[:10]
		course_list_temp.add(str(course).strip())
	#fix any formatting errors and sort course list
	for course in course_list_temp:
		if len(course) > 10:
			courseA = course[:10]
			courseB = course[10:]
			course_list.add(courseA)
			if len(courseB) >= 10 and courseB[0:3]== 'AS.' or courseB[0:3] == 'EN.':
				course_list.add(courseB)
		else: 
			course_list.add(course)
	return course_list
