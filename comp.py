import wx
import wx.lib.intctrl
import wx.lib.scrolledpanel
import wx.lib.agw.aui as aui
import threading
from Queue import Queue
import scrape

#Queue containing results of thread operations
compQueue = Queue()

#list of divisions
divs = ['Kreiger School of Arts and Sciences', 'Whiting School of Engineering']

#list of departments within KSAS
ksas = ['Africana Studies', 'Anthropology', 'Archaeology', 'Behavioral Biology', 'Bioethics', 'Biology',
'Biophysics', 'Chemistry', 'Classics', 'Cognitive Science', 'Earth and Planetary Sciences',
'East Asian Studies', 'Economics', 'English', 'Film and Media Studies', 'German and Romance Languages and Literatures',
'History', 'History of Art', 'History of Science and Technology', 'Humanities Center',
'Interdisciplinary Studies', 'International Studies', 'Jewish Studies Program', 'Language Education',
'Latin American Studies', 'Mathematics', 'Medicine, Science, and the Humanities', 'Military Science',
'Museums and Society', 'Music', 'Natural Sciences Area', 'Near Eastern Studies', 'Neuroscience',
'Philosophy', 'Physics and Astronomy', 'Political Science', 'Psychological and Brain Sciences',
'Public Health Studies', 'Social Policy', 'Sociology', 'Space Science and Engineering',
'Study of Women, Gender, and Sexuality', 'Theatre Arts and Studies', 'Visual Arts', 'Writing Seminars']

#list of departments within WSE
wse = ['Applied Mathematics and Statistics', 'Biomedical Engineering', 'Center for Leadership Education',
'Chemical and Biomolecular Engineering', 'Civil Engineering', 'Computational Medicine',
'Computer Science', 'Electrical and Computer Engineering', 'Engineering Management',
'Entrepreneurship and Management', 'General Engineering', 'Geography and Environmental Engineering', 'Information Security Institute',
'Marketing and Communications', 'Materials Science and Engineering', 'Mechanical Engineering',
'NanoBio Technology', 'Professional Communication', 'Robotics and Computational Sensing']


'''
Allows the user to select a department and class. Up to four class panels can be open at one time
'''
class ClassPanel(wx.Panel):

	def __init__(self, parent, id, panel_num = 1):
		wx.Panel.__init__(self, parent, id)
		headerFont = wx.Font(12, wx.FONTFAMILY_ROMAN, style = wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD, underline=True)
		header = wx.StaticText(self, -1, 'Class ' + str(panel_num))
		header.SetFont(headerFont)
		self.parent = parent
		self.ready = False
		self.divBox = wx.ComboBox(self, -1, 'Select a School', choices = divs)
		self.deptBox = wx.ComboBox(self, -1, 'Select a Program', choices = [])
		self.sizer = wx.GridBagSizer(5)
		self.sizer.Add(header, (0, 1))
		self.sizer.Add(self.divBox, (1, 1))
		self.sizer.Add(self.deptBox, (2, 1))
		self.divBox.Bind(wx.EVT_COMBOBOX, self.onDeptChoice)
		self.deptBox.Bind(wx.EVT_COMBOBOX, self.onClassChoice)
		self.SetBackgroundColour('LIGHT_GRAY')
		self.SetSizer(self.sizer)

	def onDeptChoice(self, evt):
		self.ready = False
		choice = self.divBox.GetValue()
		if choice == divs[0]:
			courses = ksas
		else:
			courses = wse
		self.deptBox.Clear()
		for course in courses:
			self.deptBox.Append(course)
		self.parent.parentSizer.Layout()

	def onClassChoice(self, evt):
		self.ready = True

	def isReady(self):
		return self.ready

''' IN PROGRESS - output results to GUI instead of console'''
class ResultPanel(wx.lib.scrolledpanel.ScrolledPanel):

	def __init__(self, parent, id):
		super(ResultPanel, self).__init__(parent, id)
		self.sizer = wx.BoxSizer(wx.VERTICAL)
		self.SetSizer(self.sizer)


	def set_text(self, new_text):
		self.text = wx.StaticText(self, -1, new_text)
		self.sizer.Add(self.text, wx.EXPAND | wx.TOP | wx.LEFT)
		self.SetupScrolling()

'''
Contains buttons allowing user to add up to four classes and begin the program comparison
'''
class SelectPanel(wx.Panel):

	def __init__(self, parent, id):
		wx.Panel.__init__(self, parent, id)
		self.frame = parent
		self.parentSizer = wx.BoxSizer(wx.VERTICAL)
		self.panelSizer = wx.GridSizer(rows=5, cols=2, hgap=5, vgap=15)
		self.buttonSizer = wx.GridSizer(rows=1, cols=10, hgap=5, vgap=5)
		self.parentSizer.Add(self.panelSizer, 0, wx.EXPAND)
		self.parentSizer.Add(self.buttonSizer, 0, wx.ALIGN_BOTTOM)
		self.index = 0
		self.panels = []
		init_panel = ClassPanel(self, -1)
		self.panels.append(init_panel)
		self.addButton = wx.Button(self, -1, '+')
		self.minusButton = wx.Button(self, -1, '-')
		self.startButton = wx.Button(self, -1, 'START!')
		self.addButton.Bind(wx.EVT_BUTTON, self.onAddButton)
		self.minusButton.Bind(wx.EVT_BUTTON, self.onMinusButton)
		self.startButton.Bind(wx.EVT_BUTTON, self.compare)
		self.panelSizer.Add(self.panels[0], 1, wx.EXPAND)
		self.buttonSizer.Add(self.minusButton, 1, wx.EXPAND)
		self.buttonSizer.Add(self.addButton, 1, wx.EXPAND)
		self.buttonSizer.Add(self.startButton, 1, wx.EXPAND)
		self.SetBackgroundColour('LIGHT_GRAY')
		self.SetSizer(self.parentSizer)

	def onAddButton(self, evt):
		if (len(self.panels) < 4):
			temp_panel = ClassPanel(self, -1, len(self.panels) + 1)
			self.panels.append(temp_panel)
			self.panelSizer.Add(self.panels[len(self.panels) - 1], 1, wx.EXPAND)
			self.parentSizer.Layout()

	def onMinusButton(self, evt):
		if (len(self.panels) > 1):
			self.panelSizer.Hide(self.panels[len(self.panels) - 1])
			self.panelSizer.Detach(self.panels[len(self.panels) - 1])
			self.panels.pop()
			self.parentSizer.Layout()

	def compare(self, evt):
		depts = []
		threads = []
		for i in range (0, len(self.panels)):
			div = self.panels[i].divBox.GetValue()
			dept = self.panels[i].deptBox.GetValue()
			depts.append(dept)
			if div == 'Kreiger School of Arts and Sciences':
				div = 'arts-sciences'
			elif div == 'Whiting School of Engineering':
				div = 'engineering'
			dept = dept.lower()
			dept = dept.replace('and', '')
			dept = dept.replace('  ', ' ')
			dept = dept.replace(' ', '-')
			t = CompareThread(div, dept, compQueue)
			threads.append(t)
			threads[len(threads) - 1].start()
		for thread in threads:
			thread.join()
		int_set = compQueue.get()
		while not compQueue.empty():
			int_set = int_set.intersection(compQueue.get())
		course_list = list(int_set)
		course_list.sort()
		results = ''
		for course in course_list:
			print course
			results += (' ' + course + '\n')
		self.frame.nb.EnableTab(1, True)
		self.frame.result_panel.set_text(results)

class MainFrame(wx.Frame):

	def __init__(self, parent, id, title):
		wx.Frame.__init__(self, parent, id, title, size=(640, 550), style=wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
		style = aui.AUI_NB_DEFAULT_STYLE ^ aui.AUI_NB_CLOSE_ON_ACTIVE_TAB
		nb = aui.AuiNotebook(self, agwStyle=style)
		self.select_panel = SelectPanel(self, -1)
		self.result_panel = ResultPanel(self, -1)
		nb.AddPage(self.select_panel, 'Select')
		nb.AddPage(self.result_panel, 'Results')
		nb.SetSelection(0)
		nb.EnableTab(1, False)
		self.nb = nb
		self.select_panel.SetFocus()
	
	
class CompareThread(threading.Thread):

	def __init__(self, div, dept, q):
		threading.Thread.__init__(self)
		self.div = div
		self.dept = dept
		self.q = q

	def run(self):
		val = scrape.get_courses(self.div, self.dept)
		self.q.put(val)
		
class CourseCompApp(wx.App):

	def __init__(self, output):
		wx.App.__init__(self, output)
		windowtitle = 'JHU Major Comparison Tool (Version ' + scrape.version + ')'
		self.frame = MainFrame(None, -1, windowtitle)
		self.SetTopWindow(self.frame)
		self.frame.Show(True)

def startup():
	app = CourseCompApp(False)
	app.MainLoop()

if __name__ == '__main__':
	startup()